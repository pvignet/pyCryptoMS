################################################
#                                              #
#     SAT SOLVER COMPILATION INSTRUCTIONS      #
#                                              #
#        http://cadbiom.genouest.org/          #
#                                              #
################################################



# Compilation

The aim of this package is to provide a Python library
based on a SAT solver written in C++ for Cadbiom


## System requirements

* python2.7-dev
* swig or swig3.0


## Build the Python package

In the root of the project, run the following command:

    make

Two directories will appear:

* build/lib.linux-x86_64-2.7: You will find here the compiled library and the Python module.
* dist: You will find here an archive (pyCryptoMS-0.1.tar.gz) that is ready to be distributed
through pypi repository for example.


## Compilation with CMake (obsolete)

In the root of the project, run the following command:

    mkdir build
    cd build
    cmake ..
    make

It generates pyCryptoMS.py and _pyCryptoMS.so
in build/src/binding/

Paste these both files in the solver folder
that you have defined in your pythonpath variable.

Example:

    export PYTHONPATH=$PYTHONPATH:/my_path/cadbiom/solver


# Installation

Via pip (highly recommanded because it is the unique solution which ensures
a clean and complete deletion of files) or via distutils on your system;
If you want to use this last solution, run the following command:

    make install

This command create `files.txt` which allows a full deletion of the package with:

    make uninstall


# Utilization

In a Python interpreter:

    import pyCryptoMS


# License

CADBIOM software uses CryptoMiniSat, an MIT-licensed SAT solver
(see http://www.msoos.org/cryptominisat2/).

PyCryptoMS is released under GPLv3 license.