import unittest
import sys
sys.path.append('../../../build/src/Binding')

from pyCryptoMS import *


class TestSolv(unittest.TestCase):

    def setUp(self):
        self.solv = CryptoMS() 
        c1 = [1, -2, 3]
        self.solv.addClause(c1)
        c2 = [-1,-2,-3]
        self.solv.addClause(c2)

    
        
    def tearDown(self):
        pass


##     def testSimple(self):
##         sol = self.solv.solve()
##         print "SOL: ",sol

    def testSolvMultiple(self):
        msol =  self.solv.msolve(100)
        print '\nMSOLV',msol
        print 'nbsol', len(msol)

    def testMsolvSelected(self):
        smsol =  self.solv.msolve_selected(100, [1,2])
        print '\nSMSOLV',smsol
        print 'nbsol', len(smsol)

if __name__ == "__main__":
    unittest.main()
##     suite = unittest.TestSuite()
##     suite.addTest(TestSolv("testNextSol_qdd"))
##     unittest.TextTestRunner(verbosity=2).run(suite)
