
#include "CryptoMS.h"

using namespace CMSat;

int main(int argc, char** argv)
{
  std::vector<string> opt;
  std::vector<int> iclause;
  std::vector<std::vector<int> > msol;
  std::vector<int> sol;
  CryptoMS cms;

  printf("AVANT \n");
  //cms = new CryptoMS();
  opt.push_back("verbosity=0");
  cms.set_options(opt);
  
  // add clause
//   iclause.clear();
//   iclause.push_back(1);
//   iclause.push_back(-2);
//   iclause.push_back(3);
//   cms.addClause(iclause);

//   iclause.push_back(5);
//   iclause.push_back(-6);
//   iclause.push_back(7);

//   cms.add_clause(iclause);

  cms.read_in_a_file("/home/michel/SAT/examples/bmc/bmc-ibm-11.cnf");
  cms.print_nb_clauses_vars();
//   sol = cms.solve();
//   for(uint32_t i; i<sol.size(); i++)
//     printf(" v = %i,",sol[i]); 

  msol = cms.msolve(3);
  printf("solution size %i\n",msol.size());
  for(uint32_t j; j<msol.size(); j++)
    printf("NB composantes: %i\n", msol[j].size());
//     for(uint32_t i; i<msol[j].size(); i++)
//       printf(" v = %i,",msol[j][i]); 
//   printf("Problem satisfiability: %i\n", cms.is_satisfiable());
  printf("APRES \n");
}
