**pyCryptoMS is a library for CADBIOM software.
It uses [CryptoMiniSat](http://www.msoos.org/cryptominisat2/), an MIT-licensed SAT solver.

CADBIOM is an open source modelling software. Based on Guarded transition semantic,
it gives a formal framework to help the modelling of biological systems such as cell
signaling network.**


Snapshots of CADBIOM graphical interface and model represenation are presented in
[Screenshots section](http://cadbiom.genouest.org/cw_screenshots.html).

To get more informations about CADBIOM utilization, go to
[Support section](http://cadbiom.genouest.org/cw_support.html).

Finally, to know more about the project and people in charge,
[About section](http://cadbiom.genouest.org/cw_about.html) will help you.
